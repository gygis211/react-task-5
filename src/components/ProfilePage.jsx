import React, { useState, useContext } from "react";
// import { connect } from "react-redux";
import Context from "../utils/context.js";
import Input from './Input';

function ProfilePage(props) {
    const [editedName, setName] = useState('');
    const [editedSurname, setSurname] = useState('');
    const [editedCard, setCard] = useState('');
    const context = useContext(Context)

    const handleEdit = () => {
        let user = {
            name: editedName ? editedName : context.userState.name,
            surname: editedSurname ? editedSurname : context.userState.surname,
            card: editedCard ? editedCard : context.userState.card,
        }

        context.editUser(user)

        setName('')
        setSurname('')
        setCard('')

    }

    return (

        <div>
            <h1>Данные пользователя</h1>
            <div className="post">
                <div className="inputForm">
                    <strong>Имя: <br />{context.userState.name}</strong><br />
                    <strong>Фамилия: <br />{context.userState.surname}</strong><br />
                    <strong>Номер карты: <br />{context.userState.card}</strong>
                </div>
                Имя <Input className="inputForm" value={editedName} setValue={setName} />
                фамилия <Input className="inputForm" value={editedSurname} setValue={setSurname} />
                Номер карты <Input className="inputForm" value={editedCard} setValue={setCard} flag={true}/>
                <button onClick={handleEdit}>Изменить</button>
            </div>
        </div>
    )
}

// const mapStateToProps = (state) => ({
//     profile: state.profile
// });

// const mapDispatchToProps = (dispatch) => ({
//     editUser: (data) => { dispatch({ type: 'EDIT_USER', data }) },
// });

// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(ProfilePage);

export default ProfilePage;