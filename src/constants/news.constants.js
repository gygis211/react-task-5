export const ADD_NEWS = "ADD_NEWS";
export const DELETE_NEWS = "DELETE_NEWS";
export const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";
export const EDIT_NEWS = "EDIT_NEWS";
export const FIND_NEWS = "FIND_NEWS";